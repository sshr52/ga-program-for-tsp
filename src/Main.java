import java.io.IOException;

public class Main {
    static int genomeSize = 200;
    static int maxGen = 200;
    static double pcMin = 0.0;
    static double pcMax = 0.9;
    static double pcStep = 0.05;
    static boolean protectBestCrossOver = true;
    static int crossOverGreedy = 1;
    static int crossOverNormal = 2;
    static int crossOverRandom = 0;
    static double pmMin = 0.00;
    static double pmMax = 0.2;
    static double pmStep = 0.005;
    static boolean protectBestMutation = true;
    static int mutationSwap = 1;
    static int mutationSwitch = 2;
    static int mutationRandom = 0;
    static int replication_best_double_50 = 1;
    static int replication_best_double = 2;
    static int replication_random = 0;

    public static void main(String [] args) throws IOException, InterruptedException {
        String filepath = "src/05-map-10x10-36-dist42.64.txt";
        String filepath2 = "src/06-map-100x100-50.txt";
        TSP.run_new(filepath2, "tsp.txt", 42, genomeSize, maxGen, 1, true, pcMin, pcMax, pcStep, 1, true, pmMin, pmMax, pmStep, 1, true);

    }
}

