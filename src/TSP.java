import java.awt.*;
import java.io.*;
import java.util.*;

public class TSP {
    private static Random random = new Random();
    private int [][] tspmap;
    private int [] cities;
    private double [][] distanceMatrix;
    private TreeMap<Integer, Point> citiesLocation;
    private LinkedList<int []> genome;
    private int generations;
    private int genomeSize;
    /*
    Constructors #####################################################################
     */
    public TSP(String filepath) throws IOException {
        tspmap = parseMAP(filepath);
        citiesLocation = new TreeMap<>();
        cities = findCities();
        distanceMatrix = findDistanceMap();
    }
    public TSP(int [][] map){
        tspmap = map;
        citiesLocation = new TreeMap<>();
        cities = findCities();
        distanceMatrix = findDistanceMap();
    }
    public TSP(int len, int numberOfCities){
        tspmap = new int[len][len];
        int citiesLeft = numberOfCities;
        while(citiesLeft > 0){
            int posX = random.nextInt(len-1);
            int posY = random.nextInt(len-1);
            if(tspmap[posX][posY] == 0){
                tspmap[posX][posY] = citiesLeft;
                citiesLeft--;
            }
        }
        citiesLocation = new TreeMap<>();
        cities = findCities();
        distanceMatrix = findDistanceMap();
    }
    /*
    Constructors Helper Methods #######################################################
     */
    private int [] parseLine(String str){
        ArrayList<Integer> list = new ArrayList<>();
        String prev = "";
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i)==' '){
                if(prev.equals("") == false) list.add(Integer.parseInt(prev));
                prev = "";
                continue;
            }
            if(Character.isDigit(str.charAt(i))){
                prev = prev + str.charAt(i);
                if(i == str.length()-1){
                    list.add(Integer.parseInt(prev));
                }
            }
        }
        int [] mapLine = new int[list.size()];
        for(int i = 0; i< mapLine.length; i++){
            mapLine[i] = list.get(i);
        }
        return mapLine;
    }
    private int[] findCities(){
        ArrayList <Integer> cities = new ArrayList<>();
        for(int i = 0; i< this.tspmap.length; i++){
            for(int j = 0; j < this.tspmap[i].length; j++){
                if(tspmap[i][j] != 0){
                    cities.add(tspmap[i][j]);
                    citiesLocation.put(tspmap[i][j], new Point(i,j));
                }
            }
        }
        int [] citiesArr = new int[cities.size()];
        for(int i=0; i< citiesArr.length; i++){
            citiesArr[i] = cities.get(i);
        }
        Arrays.sort(citiesArr);
        return citiesArr;
    }
    private int [][] parseMAP(String filepath) throws IOException {
        ArrayList<int []> list = new ArrayList<>();
        File file = new File(filepath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            list.add(parseLine(line));
        }
        int [][] map = new int[list.size()][list.get(0).length];

        for(int i = 0; i < map.length; i++){
            map[i] = list.get(i);
        }

        return map;
    }
    private double findDistance(Point a, Point b){
        return Math.round((Math.sqrt(Math.pow(a.x-b.x,2)+Math.pow(a.y-b.y,2)))*1000.0)/1000.0;
    }
    private double [][] findDistanceMap(){
        double [][] distanceMAP = new double[cities.length + 1][cities.length +1];
        for (int i = 1; i<distanceMAP.length; i++){
            distanceMAP[i][0] = cities[i-1];
            distanceMAP[0][i] = cities[i-1];
        }
        for(int i = 1; i < distanceMAP.length; i++){
            for(int j = 1; j < distanceMAP[i].length; j++){
                distanceMAP[i][j] = findDistance(citiesLocation.get((int)distanceMAP[i][0]), citiesLocation.get((int)distanceMAP[0][j]));
            }
        }
        return distanceMAP;
    }
    /*
    Getter #######################################################################
     */
    public int getGenerations() {
        return generations;
    }
    public int[] getCities() {
        return cities;
    }
    public int[][] getTspmap() {
        return tspmap;
    }
    public double[][] getDistanceMatrix() {
        return distanceMatrix;
    }
    public LinkedList<int[]> getGenome() {
        return genome;
    }
    public TreeMap<Integer, Point> getCitiesLocation() {
        return citiesLocation;
    }
    /*
    Printers #########################################################################
    */
    public void printTSPMAP(){
        for(int [] i : tspmap){
            System.out.println(Arrays.toString(i));
        }
    }
    public void printDistaceMatrix(){
        for(double [] i : distanceMatrix){
            System.out.println(Arrays.toString(i));
        }
    }
    public void printCities(){
        System.out.println(Arrays.toString(cities));
    }
    public void printGenome(){
        LinkedList <int []> genome = getSortedGenome(this.genome);
        for(int [] gene : genome){
            System.out.println(Math.round((getGeneFitness(gene)*1000.00))/1000.00 + " -> " + Arrays.toString(gene));
        }
    }
    /*
    Helper methods ##################################################################
     */
    public double getGeneFitness(int [] gene){
        double geneFitness = 0;
        for(int i = 0; i < gene.length-1; i++){
            geneFitness = geneFitness + findDistanceCity(gene[i], gene[i+1]);
        }
        geneFitness = geneFitness + findDistanceCity(gene[gene.length-1], gene[0]);
        return geneFitness;
    }
    public double findDistanceCity(int cityA, int cityB){
        int posCityA = 0;
        int posCityB = 0;
        for(int x = 1; x < distanceMatrix.length; x++){
            if(distanceMatrix[x][0]==cityA) posCityA = x;
            if(distanceMatrix[x][0]==cityB) posCityB = x;
            if(posCityA != 0 && posCityB != 0) break;
        }
        return distanceMatrix[posCityA][posCityB];
    }
    public LinkedList<int[]> getSortedGenome(LinkedList<int []> genome){
        LinkedList <int []> sortedGenome = new LinkedList<>();
        for(int [] gene : genome){
            if(sortedGenome.isEmpty()){
                sortedGenome.add(gene);
                continue;
            }
            boolean genePut = false;
            for(int i = 0; i < sortedGenome.size(); i++){
                if(getGeneFitness(gene) <= getGeneFitness(sortedGenome.get(i))){
                    sortedGenome.add(i,gene);
                    genePut = true;
                    break;
                }
            }
            if(!genePut) sortedGenome.add(gene);
        }
        return sortedGenome;
    }
    public void sortGenome(){
        this.genome = getSortedGenome(genome);
    }
    public boolean isIllegalGene(int [] gene){
        for (int i = 0; i< gene.length; i++){
            for(int j = 0; j<gene.length; j++){
                if(i==j) continue;
                if(gene[i]==gene[j]) return true;
            }
        }
        for(int city : cities){
            boolean cityFound = false;
            for(int geneCity : gene){
                if(city == geneCity) cityFound = true;
            }
            if(cityFound == false) return true;
        }
        return false;
    }
    public int[] getSecondBestGene(){
        int [] bestGene = getBestGene();
        double bestFitness = getGeneFitness(bestGene);
        int [] secondBestGene = bestGene;
        double secondBestFitness = Integer.MAX_VALUE;
        for(int i = 0; i < getGenome().size(); i++){
            if(Arrays.equals(bestGene, genome.get(i))) continue;
            if(secondBestFitness > getGeneFitness(getGenome().get(i))){
                secondBestFitness = getGeneFitness(getGenome().get(i));
                secondBestGene = getGenome().get(i);
            }
        }
        return secondBestGene;
    }
    // Cross Over Helper
    private Integer getNextCity(ArrayList <Integer> child, int [] gene){
        int nextCityPos = -1;
        int lastCity = child.get(child.size()-1);
        for(int i = 0; i< gene.length; i++){
            if(gene[i]==lastCity){
                if(i+1 >=gene.length) nextCityPos = 0;
                else nextCityPos = i+1;
                break;
            }
        }
        try {
            return gene[nextCityPos];
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Exception");
            System.out.println(child.toString());
            System.out.println(Arrays.toString(gene));
            System.out.println(lastCity);
            System.out.println(nextCityPos);
            return gene[nextCityPos];
        }
    }
    private boolean cityInChild(ArrayList <Integer> child, int city){
        for(int childCity : child){
            if(city == childCity) return true;
        }
        return false;
    }
    public double getBestGenomeFitness(){
        double bestFitness = Integer.MAX_VALUE;
        for(int [] gene : getGenome()){
            if(getGeneFitness(gene) < bestFitness){
                bestFitness = getGeneFitness(gene);
            }
        }
        return bestFitness;
    }
    public String getFittestGene(){
        double bestFitness = Integer.MAX_VALUE;
        int [] bestGene = new int[getCities().length];
        for(int [] gene : getGenome()){
            if(getGeneFitness(gene) < bestFitness){
                bestFitness = getGeneFitness(gene);
                bestGene = gene;
            }
        }
        return Math.round(bestFitness*1000.00)/1000.00 + " -> " + Arrays.toString(bestGene);
    }
    public int [] getBestGene(){
        double bestFitness = Integer.MAX_VALUE;
        int [] bestGene = new int[getCities().length];
        for(int [] gene : getGenome()){
            if(getGeneFitness(gene) < bestFitness){
                bestFitness = getGeneFitness(gene);
                bestGene = gene;
            }
        }
        return bestGene;
    }
    /*
    Main Methods #########################################################################
     */
    public void initGenome(int genomeSize){
        this.genomeSize = genomeSize;
        genome = new LinkedList<>();
        ArrayList <Integer> cities = new ArrayList<>();
        for(int i = 0; i < this.cities.length; i++){
            cities.add(this.cities[i]);
        }
        for (int i = 0; i<genomeSize; i++){
            Collections.shuffle(cities);
            int [] gene = new int[cities.size()];
            for(int j = 0; j < gene.length; j++){
                gene[j] = cities.get(j);
            }
            genome.add(gene);
        }
    }
    // Mutation ###############################################
    public void mutateGene_switch(int [] gene, int rate){
        for(int i = 0; i<rate; i++){
            int randomPos = random.nextInt(gene.length);
            if(randomPos == gene.length-1){
                int city1 = gene[randomPos];
                gene[randomPos] = gene[0];
                gene[0] = city1;
                continue;
            }
            int city1 = gene[randomPos];
            gene[randomPos] = gene[randomPos+1];
            gene[randomPos + 1] = city1;
        }
    }
    public int [] mutateGene_swap(int [] gene, int rate){
        int [] mutatedGene = gene.clone();
        for(int i = 0; i<rate; i++){
            int x1 = random.nextInt(gene.length);
            int x2 = random.nextInt(gene.length);
            int cityA = mutatedGene[x1];
            mutatedGene[x1] = mutatedGene[x2];
            mutatedGene[x2] = cityA;
        }
        return mutatedGene;
    }
    public void mutateGenome_switch_new(double rate, boolean protectBest){
        if(genome==null) return;
        int intRate = (int)Math.round(rate*cities.length);
        boolean bestProtected = false;
        int [] bestGene = getBestGene();
        for(int i = 0; i < genome.size(); i++){
            if(protectBest && !bestProtected){
                if(Arrays.equals(genome.get(i), bestGene)){
                    bestProtected = true;
                    continue;
                }
            };
            mutateGene_switch(genome.get(i), intRate);
        }
    }
    public void mutateGenome_swap_new(double rate, boolean protectBest){
        if(genome==null) return;
        int bestGeneIndex = 0;
        if(protectBest){
            int [] bestGene = getBestGene();
            for(int [] gene : genome){
                if(Arrays.equals(gene, bestGene)){
                    break;
                }
                bestGeneIndex++;
            }
        }
        int intRate = (int)Math.round(rate*cities.length);
        for(int i = 0; i < genome.size(); i++){
            if(protectBest && i==bestGeneIndex) continue;
            genome.set(i, mutateGene_swap(genome.get(i), intRate)) ;
        }
    }
    public void mutateGenome_switch(double rate, boolean protectBest){
        if(genome==null) return;
        int intRate = (int)Math.round(rate*cities.length);
        if(protectBest) sortGenome();
        for(int i = 0; i < genome.size(); i++){
            if(protectBest && i==0) continue;
            mutateGene_switch(genome.get(i), intRate);
        }
    }
    public void mutateGenome_swap(double rate, boolean protectBest){
        if(genome==null) return;
        if(protectBest) sortGenome();
        int intRate = (int)Math.round(rate*cities.length);
        for(int i = 0; i < genome.size(); i++){
            if(protectBest && i==0) continue;
            genome.set(i, mutateGene_swap(genome.get(i), intRate)) ;
        }
    }
    // CrossOver #############################################
    public int [] crossOverGene_normal(int [] gene1, int [] gene2){
        int [] child = new int[cities.length];
        int randomPoint = random.nextInt(cities.length);
        for(int i = 0; i<randomPoint;i++){
            child[i] = gene1[i];
        }
        for(int i = randomPoint; i < child.length; i++){
            for(int gene2City : gene2){
                boolean notinChild = true;
                for(int childCity : child){
                    if(childCity == gene2City) {
                        notinChild = false;
                        break;
                    }
                }
                if(notinChild){
                    child[i] = gene2City;
                    break;
                }
            }
        }
        return child;
    }
    public void crossOverGenome_normal(double rate, boolean protectBest){
        if(genome==null) return;
        if(genome.size()-1 == 0) return;
        int intRate = (int)Math.round(rate*genome.size());
        LinkedList <int []> childGenome = new LinkedList<>();
        if(protectBest) sortGenome();
        for(int i = 0; i < intRate; i++){
            int randomPos1 = random.nextInt(genome.size());
            int randomPos2 = random.nextInt(genome.size());
            if(protectBest){
                while(randomPos1==0 || randomPos1==0 || randomPos1 == randomPos2){
                    randomPos1 = random.nextInt(genome.size());
                    randomPos2 = random.nextInt(genome.size());
                }
            }
            while (randomPos1==randomPos2){
                randomPos1 = random.nextInt(genome.size());
                randomPos2 = random.nextInt(genome.size());
            }
            int [] child1 = crossOverGene_normal(getGenome().get(randomPos1), getGenome().get(randomPos2));
            int [] child2 = crossOverGene_normal(getGenome().get(randomPos2), getGenome().get(randomPos1));
            if(getGeneFitness(child1) < getGeneFitness(child2)){
                childGenome.add(child1);
            }
            else if (getGeneFitness(child2) < getGeneFitness(child1)){
                childGenome.add(child2);
            }
            else{
                childGenome.add(child1);
                childGenome.add(child2);
            }
//            childGenome.add(crossOverGene_normal(getGenome().get(randomPos1), getGenome().get(randomPos2)));
        }
        while (childGenome.size() < getGenome().size()){
            childGenome.add(getGenome().get(random.nextInt(genome.size())));
        }
        this.genome = childGenome;
    }
    public void crossOverGenome_normal_new(double rate, boolean protectBest){
        if(genome==null) return;
        if(genome.size()-1 <= 1) return;
        int intRate = (int)Math.round(rate*genome.size());
        LinkedList <int []> childGenome = new LinkedList<>();
        boolean bestProtected = false;
        int [] bestGene = getBestGene();
        if(protectBest){
            int bestGeneIndex = 0;
            for(int i = 0; i < genome.size(); i++){
                if(Arrays.equals(bestGene, genome.get(i))){
                    bestGeneIndex = i;
                    break;
                }
            }
            getGenome().remove(bestGeneIndex);
            getGenome().add(0, bestGene);
        }
        for(int i = 0; i < intRate; i++){
            int randomPos1 = random.nextInt(genome.size());
            int randomPos2 = random.nextInt(genome.size());
            if(protectBest){
                while(randomPos1==0 || randomPos1==0 || randomPos1 == randomPos2){
                    randomPos1 = random.nextInt(genome.size());
                    randomPos2 = random.nextInt(genome.size());
                }
            }
            while (randomPos1==randomPos2){
                randomPos1 = random.nextInt(genome.size());
                randomPos2 = random.nextInt(genome.size());
            }
            int [] child1 = crossOverGene_normal(getGenome().get(randomPos1), getGenome().get(randomPos2));
            int [] child2 = crossOverGene_normal(getGenome().get(randomPos2), getGenome().get(randomPos1));
            if(getGeneFitness(child1) < getGeneFitness(child2)){
                childGenome.add(child1);
            }
            else if (getGeneFitness(child2) < getGeneFitness(child1)){
                childGenome.add(child2);
            }
            else{
                childGenome.add(child1);
                childGenome.add(child2);
            }
//            childGenome.add(crossOverGene_normal(getGenome().get(randomPos1), getGenome().get(randomPos2)));
        }
        while (childGenome.size() < getGenome().size()){
            childGenome.add(getGenome().get(random.nextInt(genome.size())));
        }
        this.genome = childGenome;
    }
    public int [] crossOverGene_greedy(int [] gene1, int [] gene2){
        if(Arrays.equals(gene1, gene2)) return gene1;
        ArrayList <Integer> child = new ArrayList<>();
        HashSet <Integer> notInChild = new HashSet<>();
        for(int i : gene1){
            notInChild.add(i);
        }
        child.add(gene1[0]);
        notInChild.remove(gene1[0]);
        int []childGene = new int[gene1.length];
        while(child.size() < gene1.length){
            int nextCity1 = getNextCity(child, gene1);
            int nextCity2 = getNextCity(child, gene2);
            boolean city1InChild = cityInChild(child, nextCity1);
            boolean city2InChild = cityInChild(child, nextCity2);
            if(city1InChild && !city2InChild){
                child.add(nextCity2);
                notInChild.remove(nextCity2);
            }
            else if(!city1InChild && city2InChild){
                child.add(nextCity1);
                notInChild.remove(nextCity1);
            }
            else if(!city1InChild && !city2InChild){
                double distNextCity1 = findDistanceCity(child.get(child.size()-1), nextCity1);
                double distNextCity2 = findDistanceCity(child.get(child.size()-1), nextCity2);
                if(distNextCity1 == distNextCity2){
                    if(random.nextInt(10)%2==1){
                        child.add(nextCity1);
                        notInChild.remove(nextCity1);
                    }
                    else{
                        child.add(nextCity2);
                        notInChild.remove(nextCity2);
                    }
                }
                else if(distNextCity1 < distNextCity2){
                    child.add(nextCity1);
                    notInChild.remove(nextCity1);
                }
                else{
                    child.add(nextCity2);
                    notInChild.remove(nextCity2);
                }
            }
            else if(city1InChild && city2InChild){
                int randomCityIndex = 0;
                if(notInChild.size() > 1) randomCityIndex = random.nextInt(notInChild.size()-1);
                int count = 0;
                int ct = 0;
                for (int city : notInChild){
                    if(randomCityIndex == count){
                        ct = city;
                        break;
                    }
                    count++;
                }
                child.add(ct);
                notInChild.remove(ct);
            }
            for (int i = 0; i< child.size(); i++){
                for(int j = 0; j< child.size(); j++){
                    if(i == j){
                        continue;
                    }
                    if(child.get(i)==child.get(j)){
                        throw new IllegalStateException();
                    }
                }
            }
        }
        for(int i = 0; i< childGene.length; i++){
            childGene[i] = child.get(i);
        }
        return childGene;
    }
    public void crossOverGenome_greedy_new(double rate, boolean protectBest){
        if(getGenome() == null) return;
        if(getGenome().size()<=1) return;
        boolean bestProtected = false;
        int [] bestGene = getBestGene();
        if(protectBest){
            int bestGeneIndex = 0;
            for(int i = 0; i < genome.size(); i++){
                if(Arrays.equals(bestGene, genome.get(i))){
                    bestGeneIndex = i;
                    break;
                }
            }
            getGenome().remove(bestGeneIndex);
            getGenome().add(0, bestGene);

        }
        int intRate = (int)Math.round(rate*genome.size());
        LinkedList <int []> childGenome = new LinkedList<>();
        for(int i = 0; i < intRate; i++){
            int randomPos1 = random.nextInt(genome.size());
            int randomPos2 = random.nextInt(genome.size());
            if(protectBest){
                int limit = 100;
                while(randomPos1==0 || randomPos1==0 || randomPos1 == randomPos2){
                    randomPos1 = random.nextInt(genome.size()-1)+1;
                    randomPos2 = random.nextInt(genome.size()-1)+1;
                    if(limit <= 0) break;
                    limit--;
                }
            }
            else{
                int limit = 100;
                while (randomPos1==randomPos2){
                    randomPos1 = random.nextInt(genome.size());
                    randomPos2 = random.nextInt(genome.size());
                    if(limit <= 0) break;
                    limit--;
                }
            }
            int [] child1 = crossOverGene_greedy(getGenome().get(randomPos1), getGenome().get(randomPos2));
            int [] child2 = crossOverGene_greedy(getGenome().get(randomPos2), getGenome().get(randomPos1));
            if(getGeneFitness(child1) < getGeneFitness(child2)){
                childGenome.add(child1);
            }
            else if (getGeneFitness(child2) < getGeneFitness(child1)){
                childGenome.add(child2);
            }
            else{
                childGenome.add(child1);
                childGenome.add(child2);
            }
//            childGenome.add(crossOverGene_greedy(getGenome().get(randomPos1), getGenome().get(randomPos2)));
        }
        int index = 0;
        while (childGenome.size() < genomeSize){
            childGenome.add(getGenome().get(index));
            index++;
        }
        this.genome = childGenome;
    }
    public void crossOverGenome_greedy(double rate, boolean protectBest){
        if(getGenome() == null) return;
        if(getGenome().size()<=1) return;
        if(protectBest) sortGenome();
        int intRate = (int)Math.round(rate*genome.size());
        LinkedList <int []> childGenome = new LinkedList<>();
        for(int i = 0; i < intRate; i++){
            int randomPos1 = random.nextInt(genome.size());
            int randomPos2 = random.nextInt(genome.size());
            if(protectBest){
                while(randomPos1==0 || randomPos1==0 || randomPos1 == randomPos2){
                    randomPos1 = random.nextInt(genome.size()-1)+1;
                    randomPos2 = random.nextInt(genome.size()-1)+1;
                }
            }
            else{
                while (randomPos1==randomPos2){
                    randomPos1 = random.nextInt(genome.size());
                    randomPos2 = random.nextInt(genome.size());
                }
            }
            int [] child1 = crossOverGene_greedy(getGenome().get(randomPos1), getGenome().get(randomPos2));
            int [] child2 = crossOverGene_greedy(getGenome().get(randomPos2), getGenome().get(randomPos1));
            if(getGeneFitness(child1) < getGeneFitness(child2)){
                childGenome.add(child1);
            }
            else if (getGeneFitness(child2) < getGeneFitness(child1)){
                childGenome.add(child2);
            }
            else{
                childGenome.add(child1);
                childGenome.add(child2);
            }
//            childGenome.add(crossOverGene_greedy(getGenome().get(randomPos1), getGenome().get(randomPos2)));
        }
        int index = 0;
        while (childGenome.size() < genomeSize){
            childGenome.add(getGenome().get(index));
            index++;
        }
        this.genome = childGenome;
    }
    //Replication ############################################
    public void replicate_double_best_50(){
        LinkedList <int []> newGenome = new LinkedList<>();
        sortGenome();
        int [] bestGene = getGenome().get(0);
        int [] secondBestGene = getGenome().get(1);
        int index = 1;
        while(Arrays.equals(secondBestGene, bestGene)){
            secondBestGene = getGenome().get(index);
            index++;
            if(index >= getGenome().size()) break;
        }
        for(int i = 0; i< getGenome().size()/4; i++){
            newGenome.add(bestGene);
            newGenome.add(secondBestGene);
        }
        while (newGenome.size() < getGenome().size()){
            newGenome.add(getGenome().get(random.nextInt(genome.size())));
        }
        genome = newGenome;
    }
    public void replicate_double_best_50_new(){
        LinkedList <int []> newGenome = new LinkedList<>();
        int [] bestGene = getBestGene();
        int [] secondBestGene = getSecondBestGene();
        for(int i = 0; i< genomeSize/4; i++){
            newGenome.add(bestGene);
            newGenome.add(secondBestGene);
        }
        while (newGenome.size() < genomeSize){
            newGenome.add(getGenome().get(random.nextInt(genome.size())));
        }
        genome = newGenome;
    }
    public void replicate_50x2(){
        if(genome == null || genome.size() <= 1) return;
        sortGenome();
        LinkedList <int []> newGenome = new LinkedList<>();
        for(int i = 0; i < genome.size()/2; i++){
            newGenome.add(genome.get(i));
            newGenome.add(genome.get(i));
        }
        genome = newGenome;
    }
    //Next Gen
    private void nextGen(int mutationType, double mutationRate, boolean protectBestMutation, int crossOverType, double crossOverRate, boolean protectBestCrossOver, int replicationType, double bestFitness){
        if(getBestGenomeFitness() <= bestFitness) return;
        if(mutationType == 1) mutateGenome_swap(mutationRate, protectBestMutation);
        else if(mutationRate == 2) mutateGenome_switch(mutationRate, protectBestMutation);
        else{
            if(random.nextInt(2)==1) mutateGenome_switch(mutationRate, protectBestMutation);
            else mutateGenome_swap(mutationRate, protectBestMutation);
        }
        if(getBestGenomeFitness() <= bestFitness) return;
        if(crossOverType == 1) crossOverGenome_greedy(crossOverRate, protectBestCrossOver);
        else if(crossOverRate == 2) crossOverGenome_normal(crossOverRate, protectBestCrossOver);
        else{
            if(random.nextInt(2)==1) crossOverGenome_greedy(crossOverRate, protectBestCrossOver);
            else crossOverGenome_normal(crossOverRate, protectBestCrossOver);
        }
        if(getBestGenomeFitness() <= bestFitness) return;
        if(replicationType == 1)replicate_double_best_50();
        else if(replicationType == 2) replicate_50x2();
        else{
            if(random.nextInt(2)==1) replicate_double_best_50();
            else replicate_50x2();
        }
    }
    private void nextGen_new(int mutationType, double mutationRate, boolean protectBestMutation, int crossOverType, double crossOverRate, boolean protectBestCrossOver, int replicationType, double bestFitness){
        if(getBestGenomeFitness() <= bestFitness) return;
        if(mutationType == 1) mutateGenome_swap_new(mutationRate, protectBestMutation);
        else if(mutationType == 2) mutateGenome_switch_new(mutationRate, protectBestMutation);
        else if(mutationType == 0 ){}
        else{
            if(random.nextInt(2)==1) mutateGenome_switch_new(mutationRate, protectBestMutation);
            else mutateGenome_swap_new(mutationRate, protectBestMutation);
        }
        if(getBestGenomeFitness() <= bestFitness) return;
        if(crossOverType == 1) crossOverGenome_greedy_new(crossOverRate, protectBestCrossOver);
        else if(crossOverRate == 2) crossOverGenome_normal(crossOverRate, protectBestCrossOver);
        else if(crossOverRate == 0){}
        else{
            if(random.nextInt(2)==1) crossOverGenome_greedy_new(crossOverRate, protectBestCrossOver);
            else crossOverGenome_normal(crossOverRate, protectBestCrossOver);
        }
        if(getBestGenomeFitness() <= bestFitness) return;
        if(replicationType == 1)replicate_double_best_50_new();
        else if(replicationType == 2) replicate_50x2();
        else if(replicationType == 0) {}
        else{
            if(random.nextInt(2)==1) replicate_double_best_50_new();
            else replicate_50x2();
        }
    }
    public void nextGen(int iteration, int mutationType, double mutationRate, boolean protectBestMutation, int crossOverType, double crossOverRate, boolean protectBestCrossOver, int replicationType, double bestFitness ){
        if(bestFitness <= 0){
            bestFitness = 0.765 * Math.sqrt((cities.length+1)*getTspmap().length*getTspmap()[0].length);
        }
        for(int i = 0; i < iteration; i++){
            if(getBestGenomeFitness() <= bestFitness) break;
            nextGen(mutationType, mutationRate, protectBestMutation,crossOverType,crossOverRate,protectBestCrossOver,replicationType,bestFitness);
            generations++;
        }
    }
    public void nextGen_new(int iteration, int mutationType, double mutationRate, boolean protectBestMutation, int crossOverType, double crossOverRate, boolean protectBestCrossOver, int replicationType, double bestFitness ){
        if(bestFitness <= 0){
            bestFitness = 0.765 * Math.sqrt((cities.length+1)*getTspmap().length*getTspmap()[0].length);
        }
        for(int i = 0; i < iteration; i++){
            if(getBestGenomeFitness() <= bestFitness) break;
            nextGen_new(mutationType, mutationRate, protectBestMutation,crossOverType,crossOverRate,protectBestCrossOver,replicationType,bestFitness);
            generations++;
        }
    }
    private void nextGen_new_Debug(int mutationType, double mutationRate, boolean protectBestMutation, int crossOverType, double crossOverRate, boolean protectBestCrossOver, int replicationType, double bestFitness){
        if(getBestGenomeFitness() <= bestFitness) return;
        System.out.println("Mutating " + genome.size());
        if(mutationType == 1) mutateGenome_swap_new(mutationRate, protectBestMutation);
        else if(mutationType == 2) mutateGenome_switch_new(mutationRate, protectBestMutation);
        else if(mutationType == 0 ){}
        else{
            if(random.nextInt(2)==1) mutateGenome_switch_new(mutationRate, protectBestMutation);
            else mutateGenome_swap_new(mutationRate, protectBestMutation);
        }
        if(getBestGenomeFitness() <= bestFitness) return;
        System.out.println("CrossingOver " + genome.size());
        if(crossOverType == 1) crossOverGenome_greedy_new(crossOverRate, protectBestCrossOver);
        else if(crossOverRate == 2) crossOverGenome_normal(crossOverRate, protectBestCrossOver);
        else if(crossOverRate == 0){}
        else{
            if(random.nextInt(2)==1) crossOverGenome_greedy_new(crossOverRate, protectBestCrossOver);
            else crossOverGenome_normal(crossOverRate, protectBestCrossOver);
        }
        if(getBestGenomeFitness() <= bestFitness) return;
        System.out.println("Replicating " + genome.size());
        if(replicationType == 1)replicate_double_best_50_new();
        else if(replicationType == 2) replicate_50x2();
        else if(replicationType == 0) {}
        else{
            if(random.nextInt(2)==1) replicate_double_best_50_new();
            else replicate_50x2();
        }
    }
    public void nextGen_new_Debug(int iteration, int mutationType, double mutationRate, boolean protectBestMutation, int crossOverType, double crossOverRate, boolean protectBestCrossOver, int replicationType, double bestFitness ){
        if(bestFitness <= 0){
            bestFitness = 0.765 * Math.sqrt((cities.length+1)*getTspmap().length*getTspmap()[0].length);
        }
        for(int i = 0; i < iteration; i++){
            if(getBestGenomeFitness() <= bestFitness) break;
            System.out.println("Gen " + i + " " + + genome.size());
            nextGen_new_Debug(mutationType, mutationRate, protectBestMutation,crossOverType,crossOverRate,protectBestCrossOver,replicationType,bestFitness);
            generations++;
        }
    }
    public static void run(String filename, String outputFileName, double bestFitness ,int genomeSize, int maxGen, int pcType, boolean protectBestCrossOver, double pcStart, double pcEnd, double pcStep, int pmType, boolean protectBestMutation, double pmStart, double pmEnd, double pmStep, int recombineType, boolean printBest) throws IOException {

        StringBuffer outputPcPmGen = new StringBuffer("");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int bestResuls = Integer.MAX_VALUE;
                for(double pc = pcStart; pc <= pcEnd; pc = pc+pcStep){
                    for(double pm = pmStart; pm <= pmEnd; pm = pm +pmStep){
                        TSP tspMap = null;
                        try {
                            tspMap = new TSP(filename);
                            tspMap.initGenome(genomeSize);
                            String two = "Before : " + tspMap.getFittestGene();
                            tspMap.nextGen(maxGen, pmType,pm,protectBestMutation, pcType, pc, protectBestCrossOver, recombineType, bestFitness);
                            String one = "pc:" + Math.round(pc*1000.0)/1000.0 + ", pm:" + Math.round(pm*1000.0)/1000.0 + " gen:"+ tspMap.getGenerations();
                            String three = "After  : " + tspMap.getFittestGene();

                            outputPcPmGen.append(Math.round(pc*1000.0)/1000.0 + " " + Math.round(pm*1000.0)/1000.0 + " " + tspMap.getGenerations() + " " + tspMap.getFittestGene());
                            if(printBest){
                                if(tspMap.getGenerations() < bestResuls){
                                    System.out.println(one + "\n"+two + "\n"+three+"\n");
                                    bestResuls = tspMap.generations;
                                    outputPcPmGen.append("   <-------------------");
                                }
                            }
                            else{
                                System.out.println(one + "\n"+two + "\n"+three+"\n");
                            }
                            outputPcPmGen.append("\n");
                        } catch (IOException e) {
                            System.out.println("File Not Found");
                            break;
                        }
                    }
                    outputPcPmGen.append("\n");
                }
                File file = new File(outputFileName);
                try {
                    FileWriter fileWriter = new FileWriter(file, true);
                    synchronized (file){
                        fileWriter.write(outputPcPmGen.toString());
                    }
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    public static void run_new(String filename, String outputFileName, double bestFitness ,int genomeSize, int maxGen, int pcType, boolean protectBestCrossOver, double pcStart, double pcEnd, double pcStep, int pmType, boolean protectBestMutation, double pmStart, double pmEnd, double pmStep, int recombineType, boolean printBest) throws IOException, InterruptedException {
        StringBuffer outputPcPmGen = new StringBuffer("");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(java.time.LocalTime.now());
                long startTime = System.nanoTime();
                double bestResuls = Integer.MAX_VALUE;
                for(double pc = pcStart; pc <= pcEnd; pc = pc+pcStep){
                    for(double pm = pmStart; pm <= pmEnd; pm = pm +pmStep){
                        TSP tspMap = null;
                        try {
                            tspMap = new TSP(filename);
                            tspMap.initGenome(genomeSize);
                            String two = "Before : " + tspMap.getFittestGene();
                            tspMap.nextGen_new(maxGen, pmType,pm,protectBestMutation, pcType, pc, protectBestCrossOver, recombineType, bestFitness);
                            String one = "pc:" + Math.round(pc*1000.0)/1000.0 + ", pm:" + Math.round(pm*1000.0)/1000.0 + " gen:"+ tspMap.getGenerations();
                            String three = "After  : " + tspMap.getFittestGene();

                            outputPcPmGen.append(Math.round(pc*1000.0)/1000.0 + " " + Math.round(pm*1000.0)/1000.0 + " " + tspMap.getGenerations() + " " + tspMap.getFittestGene());
                            if(tspMap.getBestGenomeFitness() < bestResuls){
                                bestResuls = tspMap.getBestGenomeFitness();
                                outputPcPmGen.append("   <-------------------");
                            }
                            if(printBest){
                                if(tspMap.getGenerations() <= bestResuls){
                                    System.out.println(one + " " + java.time.LocalTime.now() +"\n"+two + "\n"+three+"\n");
                                }
                            }
                            else{
                                System.out.println(one + " " + java.time.LocalTime.now() + "\n"+two + "\n"+three+"\n");
                            }
                            outputPcPmGen.append("\n");
                        } catch (IOException e) {
                            System.out.println("File Not Found");
                            break;
                        }
                    }
                    outputPcPmGen.append("\n");
                }

                long endTime   = System.nanoTime();
                System.out.println(java.time.LocalTime.now());
                long totalTime = endTime - startTime;
                System.out.println("Total Time : " + totalTime/Math.pow(10,-9) + "Sec");
                System.out.println("Output : " + outputPcPmGen.toString());
                File file = new File(outputFileName);
                try {
                    FileWriter fileWriter = new FileWriter(file, true);
                    synchronized (file){
                        fileWriter.write(outputPcPmGen.toString());
                    }
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();


    }
    public static void test(String filename, String output,  int testLimit, int genomeSize, int maxGen, double pc, int crossOverType,boolean protectBestCrossOver, double pm, int mutationType, boolean protectBestMutation, int replicationType, double bestFitness) throws IOException {
        if(bestFitness <= 1){
            TSP tspMap1 = new TSP(filename);
            bestFitness = 0.765 * Math.sqrt((tspMap1.getCities().length+1)*tspMap1.getTspmap().length*tspMap1.getTspmap()[0].length);
            System.out.println("Determines BestFitness : " + bestFitness);
        }
        double [][] graphData = new double[testLimit][2];
        StringBuffer outputData = new StringBuffer("");
        for(int i = 0; i<testLimit; i++){
            System.out.println("################\nTest " + (i + 1)+"\n################");
            TSP tspMap = new TSP(filename);
            tspMap.initGenome(genomeSize);
            String two = "Before : " + tspMap.getFittestGene();
            tspMap.nextGen(maxGen, mutationType,pm,protectBestMutation, crossOverType, pc, protectBestCrossOver, replicationType, bestFitness);
            String one = "pc:" + Math.round(pc*1000.0)/1000.0 + ", pm:" + Math.round(pm*1000.0)/1000.0 + " gen:"+ tspMap.getGenerations();
            String three = "After  : " + tspMap.getFittestGene();
            System.out.println(one + "\n"+two + "\n"+three+"\n");
            graphData[i][0] = bestFitness;
            graphData[i][1] = tspMap.getBestGenomeFitness();
            outputData.append(tspMap.getBestGenomeFitness() + "\n");

        }
        File file = new File(output);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(outputData.toString());
        fileWriter.close();
    }
    public static void test_new(String filename, String output, int testLimit, int genomeSize, int maxGen, double pc, int crossOverType,boolean protectBestCrossOver, double pm, int mutationType, boolean protectBestMutation, int replicationType, double bestFitness) throws IOException {
        long startTime   = System.nanoTime();
        if(bestFitness <= 1){
            TSP tspMap1 = new TSP(filename);
            bestFitness = 0.765 * Math.sqrt((tspMap1.getCities().length+1)*tspMap1.getTspmap().length*tspMap1.getTspmap()[0].length);
            System.out.println("Determines BestFitness : " + bestFitness);
        }
        double [][] graphData = new double[testLimit][2];
        StringBuffer outputData = new StringBuffer("");
        for(int i = 0; i<testLimit; i++){
            System.out.println("################\nTest " + (i + 1)+"\n################");
            TSP tspMap = new TSP(filename);
            tspMap.initGenome(genomeSize);
            String two = "Before : " + tspMap.getFittestGene();
            tspMap.nextGen_new(maxGen, mutationType,pm,protectBestMutation, crossOverType, pc, protectBestCrossOver, replicationType, bestFitness);
            String one = "pc:" + Math.round(pc*1000.0)/1000.0 + ", pm:" + Math.round(pm*1000.0)/1000.0 + " gen:"+ tspMap.getGenerations();
            String three = "After  : " + tspMap.getFittestGene();
            System.out.println(one + "\n"+two + "\n"+three+"\n");
            graphData[i][0] = bestFitness;
            graphData[i][1] = tspMap.getBestGenomeFitness();
            outputData.append(tspMap.getBestGenomeFitness() + "\n");
        }
        File file = new File(output);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(outputData.toString());
        fileWriter.close();
        long endTime   = System.nanoTime();
        System.out.println(java.time.LocalTime.now());
        long totalTime = endTime - startTime;
        System.out.println(totalTime/Math.pow(10,-9) + "Sec");

    }
    public static void run_new_debug(String filename, String outputFileName, double bestFitness ,int genomeSize, int maxGen, int pcType, boolean protectBestCrossOver, double pcStart, double pcEnd, double pcStep, int pmType, boolean protectBestMutation, double pmStart, double pmEnd, double pmStep, int recombineType, boolean printBest) throws IOException {
        StringBuffer outputPcPmGen = new StringBuffer("");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(java.time.LocalTime.now());
                long startTime = System.nanoTime();
                int bestResuls = Integer.MAX_VALUE;
                for(double pc = pcStart; pc <= pcEnd; pc = pc+pcStep){
                    for(double pm = pmStart; pm <= pmEnd; pm = pm +pmStep){
                        TSP tspMap = null;
                        try {
                            tspMap = new TSP(filename);
                            tspMap.initGenome(genomeSize);
                            String two = "Before : " + tspMap.getFittestGene();
                            tspMap.nextGen_new_Debug(maxGen, pmType,pm,protectBestMutation, pcType, pc, protectBestCrossOver, recombineType, bestFitness);
                            String one = "pc:" + Math.round(pc*1000.0)/1000.0 + ", pm:" + Math.round(pm*1000.0)/1000.0 + " gen:"+ tspMap.getGenerations();
                            String three = "After  : " + tspMap.getFittestGene();

                            outputPcPmGen.append(Math.round(pc*1000.0)/1000.0 + " " + Math.round(pm*1000.0)/1000.0 + " " + tspMap.getGenerations() + " " + tspMap.getFittestGene());
                            if(tspMap.getGenerations() < bestResuls){
                                bestResuls = tspMap.generations;
                                outputPcPmGen.append("   <-------------------");
                            }
                            if(printBest){
                                if(tspMap.getGenerations() <= bestResuls){
                                    System.out.println(one + " " + java.time.LocalTime.now() +"\n"+two + "\n"+three+"\n");
                                }
                            }
                            else{
                                System.out.println(one + " " + java.time.LocalTime.now() + "\n"+two + "\n"+three+"\n");
                            }
                            outputPcPmGen.append("\n");
                        } catch (IOException e) {
                            System.out.println("File Not Found");
                            break;
                        }
                    }
                    outputPcPmGen.append("\n");
                }
                File file = new File(outputFileName);
                try {
                    FileWriter fileWriter = new FileWriter(file, true);
                    synchronized (file){
                        fileWriter.write(outputPcPmGen.toString());
                    }
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                long endTime   = System.nanoTime();
                System.out.println(java.time.LocalTime.now());
                long totalTime = endTime - startTime;
                System.out.println("Total Time : " + totalTime/Math.pow(10,-9) + "Sec");

            }
        });
        thread.start();
    }
}